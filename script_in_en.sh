#!/bin/bash

LOCAL="$(pwd)"

wait_input()
{
#Waits for input, or continues the procedure
	if [ $1 -ge 0 ]; then
		mp=$(( $TIME*$1 ))
	else
		mp=$TIME
	fi

	read -n 1 -t $mp
	if [ $? == 0 ]; then
		echo "1"
		exit
	fi
	echo "0"
	exit
}

verify_int()
{
	if ! [ -a $SHA256SUM ]; then
		echo $MISSING_SUM
		exit
	fi

	compromissed=$( sha256sum --quiet -c $SHA256SUM )	#If sha256sum retuns something, it's the warning that the file is compromissed

	if [ -z "$compromissed" ]; then	#If the variable is set, the file is compromissed
		echo $OK
	else
		echo $INTEGRITY_CHECK_FAILED
	fi
	exit
}

update_hash()
{
#Forces the hash to be updated, as well the backup file
	find -type f \( -not -name "sha256sum.txt" \) -exec sha256sum '{}' \; > $SHA256SUM	#Generate and save the new hash
}

uptodate()
{
#Update the computer, duh!
	rm -f /var/lib/dpkg/lock* #Removing apt lock, due to killing problems
	rm -f /var/lib/apt/lists/lock

	apt update
	apt -y full-upgrade
	apt -y autoremove
}

build_lists()
{
#Building apps installation and removal lists

	echo "Building apps lists"

	if [ -a $LOCAL/sys/$INSTALL ]; then	#if there a specific room list, concatenate
		cat $LOCAL/sys/$INSTALL >> /tmp/prog_tmp
	fi

	if [ -a $LOCAL/sys/$REMOVE ]; then #Read the specified room list as well
		cat $LOCAL/sys/$REMOVE >> $REMOTE/$REMOVE
	fi
	cat /tmp/prog_tmp | grep -v '$(cat $REMOTE/$REMOVE)' > $REMOTE/$INSTALL #Remove the to-remove apps from install list, due possible colision problems

	echo "Uninstalling unwanted packages"
	for i in $(cat $REMOTE/$REMOVE); do
		apt -y purge $i
	done

	#Installing listed programs in prog_ins

	echo "Installing wanted packages"
	for i in $(cat $REMOTE/$INSTALL); do
		apt -y install $i
	done

	rm $REMOTE/$INSTALL #clean up the mess
	rm $REMOTE/$REMOVE
	rm /tmp/prog_tmp
}

finish()
{
	#Removing installed suport programs, they're not needed anymore, listed in prog_aft

	echo "Removing apps installed due script operations"
	post_clear

	echo "After job's for this room, if any (specifc room scripts)"
	if [ -a $LOCAL/conf/scripts/after.sh ]; then
		bash $LOCAL/conf/scripts/after.sh
	fi
	uptodate
}

find_impostors()
{
#Serach for non-default users on the computer, and delete them
	users=$(compgen -u) #List the users names
	for user in $users; do
		useruid=$(id -u $user) #Get the uid of each user
		if [ $useruid -gt 999 ] && [ $useruid -lt 65534 ]; then #it means it's not a system/default user
			delete=$(cat $LOCAL/conf/default_users | grep $user) #Compare with the default user list
			if [ -z $delete ]; then
				deluser --remove-all-files $user
			fi
			unset delete
		fi
	done
}

user_config()
{
#Add the admin user and remove default unwanted users permissions
	echo "You may pass (adding users)"

      find_impostors

	useradd -m -r -s /bin/bash $MUSER

	echo "Just to be sure, i'll reset users passwords for you :)"

	echo "$(cat $REMOTE/$SHADOW | grep -v $MUSER )" >> /tmp/shadow

	if [ -z "$MUSER_P" ]; then
		passwd -d $MUSER
	else
		echo "$MUSER:$MUSER_P:0:0:99999:7:::" >> /tmp/shadow
	fi

	mv /tmp/shadow $REMOTE/$SHADOW
	chmod 640 $REMOTE/$SHADOW

	cat $REMOTE/$SUDOERS | grep -v %sudo | grep -v %admin > /tmp/sudoers

	if [ -a $LOCAL/sys/$SUDOERS ]; then
		cat $LOCAL/sys/$SUDOERS >> /tmp/sudoers
	else
		echo $MISSING_SUDOERS
	fi

	mv /tmp/sudoers /$SUDOERS

	echo "And you may not... (removing unecessary permissions and groups)"
	groupdel -f sudo
	groupdel -f admin
}

goodbye()
{
#GOOD-FCKN-BYE
	echo "Shuting down... press any buton to menu"
	if [ "$(wait_input 100)" = "1" ]; then
		help
		read op
		menu ${op[@]}
		exit
	fi

	echo "Goodbye :)"
	shutdown -Ph now
}

hello()
{
	echo "#######################################################"
	echo "#   		          R.A.T.A.O				#"
	echo "#   Reconhecimento Automático de Traços de Antigos	#"
	echo "#   Owners, $VERSION						#"
	echo "#   Computer configuration script, by Allan Ribeiro   #"
	echo "#     email to:<allanribeiro14atgmail.com>		#"
	echo "#######################################################"
}

menu()
{
	for parameter in $@; do
		case $parameter in
			1 )
				if [ $(verify_int) -eq 0 ]; then
					echo "A file has been modified. Verify \"compromissed\" for more details."
				else
					echo "No alterations has been found."
				fi
				;;
			2 )
				update_hash
				;;
			3 )
				update_bkp
				;;
			4 )
				user_config
				;;
			5 )
				uptodate
				;;
			6 )
				build_lists
				;;
			7 )
				package_config
				;;
			8 )
				finish
				;;
			9 )
				hello
				;;
			0 )
				exit
				;;
			* )
				echo $INVALID_OPT
				help
				exit
		esac
	done
}

default_procedure()
{

	#Users setting

	echo "Configuring users"
	user_config

	#Updating and installing needed packages

	echo "Updating system"
	uptodate

	#Building apps installation and removal lists

	echo "Building apps list"
	build_lists

	#Removing listed programs in prog_rem

	echo "Now, configuring apps"
	package_config

	#Finishing

	echo "last package verification before remove support-to-script apps"
	finish

	goodbye
}


main()
{
	if [ "$(whoami)" = "root" ]; then #Check if the user has the permission to run this script

		echo "Loading program varibles"
		if [ -a ./conf/var/script ]; then
			source ./conf/var/script
		else
			echo $MISSING_VARIABLES
			exit
		fi

		if ! [ $# -gt 0 ]; then #Check for parameters (specifc script functions). If no parameters has been given, run program normally
			echo "Verifying program's integrity"
			if [ "$(verify_int)" != 0 ]; then
				echo "A file has been modified. Exiting"
				exit
			fi

			default_procedure

		else #Call specifc script funtions
				menu $@
		fi

		exit

		hello	#Say hello, Script
		nmcli n connectivity check	#If not logged in, force the login screen to appear
		sleep $TIME

	else
		echo "Thanks for avoiding root. But this script needs it (try with sudo)"
	fi
}

main $@
