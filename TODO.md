# TODO list

 - [ ] Decide program routines

	* Verify possible bug of hierarchy:
		- check wether variables are meant to be global, or not

	* Generate current backup file
		- personal files
			Downloads
			Documents
			Musics
			Videos
			Keys
			Mails
			Games
		- program configs
			nanorc
			GUI configs
				Theme
				Wallpappers
			Browsers
				Passwords
			Tor
				Bridges?
			Evolution
				.local/share/evolution
				.config/evolution
			GPG client
			SSH client
		- User configs
			Existing users, and permissions
			Password
			Installed programs
			Removed programs
			aliases
			terminal flags
			mail client

	* Write backup in new OS

 - [ ] Write program routines

 - [ ] Debug program routines

 - [ ] Test program

 - [ ] Release program

 - [ ] Use program